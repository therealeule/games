package at.obi.Games;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Pipe implements Actor {
	private double degree, x, y, speed, x2, y2;
	private int width, height;
	private boolean left, right;
	private Image image;
	private Image image2;
	private Shape shape;
	private Shape shape2;
	public static int score;
	public static boolean score1 = false;

	public Pipe(double degree, double x, double x2, double y, double speed, int width, int height, boolean left,
			boolean right, int score) {
		this.degree = degree;
		this.x = x;
		this.x2 = x2;
		this.y = coordinateY();
		this.y2 = coordinateY2();
		this.speed = speed;
		this.width = width;
		this.height = height;
		this.left = left;
		this.right = right;
		this.score = score;

		this.shape = new Rectangle((float) this.x, (float) this.y, 60, 600);
		this.shape2 = new Rectangle((float) this.x, (float) this.y, 60, 500);

		try {
			image = new Image("images/pipe_up.png");
			image2 = new Image("images/pipe_down1.png");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void update(int delta, GameContainer gc) {
		if (gc.getInput().isKeyDown(Input.KEY_K)) {
			score = 0;

		}
		if (Player.gameOver == false) {

		if (score >= 0) {
			this.shape.setX((float) this.x);
			this.shape.setY((float) this.y);

			this.shape2.setX((float) this.x2);
			this.shape2.setY((float) this.y2);
		}
		if (score >= 5) {
			this.speed += 0.000011;
		}

		this.x -= delta * this.speed;
		if (this.x < -10) {
			this.x = 820;
			this.y = coordinateY();
			score++;
		}

		this.x2 -= delta * this.speed;
		if (this.x2 < -10) {
			this.x2 = 820;
			this.y2 = coordinateY2();
		}
		
		System.out.println(score);
		}
	}

	public void render(Graphics graphics) {
		image.draw((int) this.x, (int) this.y, 60, 600);

		

		image2.draw((int) this.x2, (int) this.y2, 60, 500);

	}

	public double coordinateY() {
		double y = (int) ((Math.random()) * 400 + 1) + 200;
		return y;

	}

	public double coordinateY2() {
		double y = this.y - 660;
		return y;
	}

	@Override
	public Shape getShape() {

		return shape;
	}

	public Shape getShape2() {
		// TODO Auto-generated method stub
		return shape2;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
