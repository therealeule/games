package at.obi.Games;

import java.awt.Container;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.BigImage;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import javax.swing.*;





public class Landscape extends BasicGame {

	private Player playerMain;

	private Image original;
	private Image start;
	private Image startHead;
	private Image gameOver;
	public static Image player1;
	public static Image player2;
	public static Image player3;
	public static Image player4;
	public static Image player5;
	public static Image player6;
	public static Image player7;
	public static Image player8;
	
	

	private Pipe pipe;
	private int count = 0;
	public boolean gameStart = false;
	public static boolean player1Bo =false;
	public static boolean player2Bo =false;
	public static boolean player3Bo =false;
	public static boolean player4Bo =false;
	public static boolean player5Bo =false;
	public static boolean player6Bo =false;
	public static boolean player7Bo =false;
	public static boolean player8Bo =false;

	public List<Actor> actors;

	public static final int WIDTH = 800;

	public static final int HEIGHT = 300;



    
    public Landscape(){
    	super("Bouncing Heads");
 
        
    }
    
   

	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		
			if (key == Input.KEY_ESCAPE) {
				System.exit(0);
			}
		
		
		
		if (key == Input.KEY_K) {
			
			this.gameStart = false;
			Player.gameOver = false;
			Pipe.score = 0;
			this.player1Bo = false;
			this.player2Bo = false;
			this.player3Bo =false;
			this.player4Bo =false;
			this.player5Bo =false;
			this.player6Bo =false;
			this.player7Bo =false;
			this.player8Bo =false;
			start();
			
		}
		}

		
		
	
	
	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (clickCount == 1 && x>=50 && x<=200 && y>=40 && y<=190) {
			this.gameStart = true;
			this.player1Bo = true;
		}
		if (clickCount == 1 && x>=233 && x<=383 && y>=40 && y<=190) {
			this.gameStart = true;
			this.player2Bo = true;
		
		}
		if (clickCount == 1 && x>=416 && x<=566 && y>=40 && y<=190) {
			this.gameStart = true;
			this.player3Bo = true;
		
		}
		if (clickCount == 1 && x>=600 && x<=750 && y>=40 && y<=190) {
			this.gameStart = true;
			this.player4Bo = true;
		}
		
		
		
		if (clickCount == 1 && x>=50 && x<=200 && y>=380 && y<=530) {
			this.gameStart = true;
			this.player5Bo = true;
		}
		if (clickCount == 1 && x>=233 && x<=383 && y>=380 && y<=530) {
			this.gameStart = true;
			this.player6Bo = true;	
		}
		if (clickCount == 1 && x>=416 && x<=566 && y>=380 && y<=530) {
			this.gameStart = true;
			this.player7Bo = true;
		}
		if (clickCount == 1 && x>=600 && x<=750 && y>=380 && y<=530) {
			this.gameStart = true;
			this.player8Bo = true;
		}
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		
		if (gameStart == false) {

			start.draw(0, 0);
			startHead.draw(0, 220);
			
			player1.draw(50, 40, 150, 150);
			player2.draw(233, 40, 150, 150);
			player3.draw(416, 40, 150, 150);
			player4.draw(600, 40, 150, 150);
			
			player5.draw(50, 380, 150, 150);
			player6.draw(233, 380, 150, 150);
			player7.draw(416, 380, 150, 150);
			player8.draw(600, 380, 150, 150);
			
		}

		if (gameStart == true) {
			original.draw(0, 0);
			graphics.drawString("Score " + Pipe.score, 710, 10);
			for (Actor actor : this.actors) {
				actor.render(graphics);
			}

		}

		if (Player.gameOver == true) {
			gameOver.draw(0, -40, 800, 800);
	
		}
		
	}
	
	private void start () {
		this.actors = new ArrayList<>();

		playerMain = new Player(40, 0, 0.3);
		this.actors.add(playerMain);

		for (int i = 0; i <= 0; i++) {
			Pipe pip = new Pipe(0, 0, 0, 0, 0.4, 24, 24, false, false, -1);
			this.actors.add(pip);
			this.playerMain.addCollissionPartner(pip.getShape());
			this.playerMain.addCollissionPartner(pip.getShape2());
		}

	}
	

	@Override
	public void init(GameContainer gc) throws SlickException {
		original = new BigImage("images/backgroundHTL.png");
		start = new BigImage("images/background.png");
		startHead = new BigImage("images/start.png");
		player1 = new Image("images/trump.png");
		player2 = new Image("images/merkel.png");
		player3 = new Image("images/kurz.png");
		player4 = new Image("images/riedmann.png");
		player5 = new Image("images/h�mmerle.png");
		player6 = new Image("images/hcStrache.png");
		player7 = new Image("images/mrbean.png");
		player8 = new Image("images/may.png");
		
		gameOver = new Image("images/gameover.png");
		gc.setShowFPS(false);
		start();
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		if (gameStart == true) {
			for (Actor actor : this.actors) {
				actor.update(delta, gc);
			}
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(800, 600, false);
			container.isShowingFPS();
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}
	}


