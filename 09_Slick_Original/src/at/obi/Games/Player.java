package at.obi.Games;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {

	private double x, y, speed;
	private Pipe pipe;
	private Shape shape;
	private int floorHeight = 100;
	private int jumpStrength = 0;
	private int weight = 1;
	private List<Shape> collisionspartner;
	public static boolean gameOver = false;
	private static int playerSizeX = 60;
	private static int playerSizey = 60;

	public Player(double x, double y, double speed) {
		super();
		this.collisionspartner = new ArrayList<>();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.shape = new Circle((float) this.x, (float) this.y, 31, 31);

		try {
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void update(int delta, GameContainer gc) {
		if (this.gameOver == false) {

		if (this.y < 537 && this.y > -5) {
			this.y += delta * this.speed;
		}


		if (gc.getInput().isKeyDown(Input.KEY_SPACE)) {

			if (this.y > 0) {
				this.y -= delta * this.speed * 2.4;

			}
		}
		

		this.shape.setX((float) this.x);
		this.shape.setY((float) this.y);

		for (Shape shape : collisionspartner) {

			if (shape.intersects(this.shape)) {

				System.out.println("Game over");
				this.gameOver = true;
			}
		}
		}
	}

	public void addCollissionPartner(Shape s) {
		this.collisionspartner.add(s);
	}

	public void render(Graphics graphics) {
		
		
		if (Landscape.player1Bo == true) {
			Landscape.player1.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player2Bo == true) {
			Landscape.player2.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player3Bo == true) {
			Landscape.player3.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player4Bo == true) {
			Landscape.player4.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player5Bo == true) {
			Landscape.player5.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player6Bo == true) {
			Landscape.player6.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player7Bo == true) {
			Landscape.player7.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
		if (Landscape.player8Bo == true) {
			Landscape.player8.draw((int) this.x, (int) this.y, playerSizeX, playerSizey);
		}
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Shape getShape2() {
		// TODO Auto-generated method stub
		return null;
	}

}
