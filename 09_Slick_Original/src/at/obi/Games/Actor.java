package at.obi.Games;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public interface Actor {
	public void update (int delta, GameContainer gc);
	public void render (Graphics graphics);
	public Shape getShape();
	public Shape getShape2();
	
}
