package at.obi.Landsacpe;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class HTLCicrle implements Actor{
	private double degree, x, y, speed;
	private int width, height;
	private boolean left, right;
	
	

	public HTLCicrle(double degree, double x, double y, double speed, int width, int height, boolean left,
			boolean right) {
		super();
		this.degree = degree;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.width = width;
		this.height = height;
		this.left = left;
		this.right = right;
	}

	public void update(int delta, GameContainer gc) {
		if (this.x <= 100) {
			this.right = true;
			this.left = false;
		}
		
		if (this.right == true) {
			this.x+=delta * this.speed;
		}
		if (this.x >= 500) {
			this.left = true;
			this.right = false;
			}
		if (this.left == true) {
			this.x-=delta * this.speed;
		}
		
	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.green);
		graphics.drawOval((int)this.x, (int)this.y, this.width, this.height);
		graphics.setColor(Color.white);
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
