package at.obi.Landsacpe;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class HTLRectangle implements Actor{
	private double degree, x, y, speed;
	private int width, height;
	private Shape shape;
	
	
	
	public HTLRectangle(double degree, double x, double y, double speed, int width, int height) {
		super();
		this.degree = degree;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.width = width;
		this.height = height;
		this.shape =  new Rectangle((float)this.x,(float) this.y, 50,50);
	}

	public void update(int delta, GameContainer gc) {

		
	      
		this.x = (int)(100 + 100 * Math.sin(this.degree += delta*this.speed));
		this.y = (int)(100 + 100 * Math.cos(this.degree += delta*this.speed));
		
		this.shape.setX((float)this.x);
		this.shape.setY((float)this.y);

	    
	}
	
	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		graphics.drawRect((int)this.x,(int)this.y, this.width, this.height);
		graphics.setColor(Color.white);
		graphics.draw(this.shape);
	}
	
	public Shape getShape() {
		return shape;
	}

	
}
