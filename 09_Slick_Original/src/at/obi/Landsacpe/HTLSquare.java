package at.obi.Landsacpe;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class HTLSquare implements Actor{
	private double degree, x, y, speed;
	private int width, height;
	private boolean left, right, up, down;

	

	public HTLSquare(double degree, double x, double y, double speed, int width, int height, boolean left,
			boolean right, boolean up, boolean down) {
		super();
		this.degree = degree;
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.width = width;
		this.height = height;
		this.left = left;
		this.right = right;
		this.up = up;
		this.down = down;
	}

	public void update(int delta, GameContainer gc) {
		if (this.x <= 100 && this.y <= 100) {
			this.right = true;
			this.left =false;
			this.up = false;
			this.down = false;
	}
		if (this.right == true) {
			this.x+= delta * this.speed;
		}
		if (this.x >= 500 && this.y <= 100) {
			this.right = false;
			this.left = false;
			this.up = false;
			this.down = true;
	}
		if (this.down == true) {
			this.y+= delta * this.speed;
		}
		if (this.x >= 500 && this.y >= 400) {
			this.right = false;
			this.left = true;
			this.up = false;
			this.down = false;
		}
	if (this.left == true) {
			this.x-= delta * this.speed;
	}
		if (this.x <= 100 && this.y >= 400) {
			this.right = false;
			this.left = false;
			this.up = true;
			this.down = false;
		}
		if (this.up == true) {
			this.y-= delta * this.speed;
	}
	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.blue);
		graphics.drawRect((int)this.x, (int)this.y, this.width, this.height);
		graphics.setColor(Color.white);
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
