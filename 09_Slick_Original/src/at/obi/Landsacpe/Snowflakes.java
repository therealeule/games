package at.obi.Landsacpe;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class Snowflakes implements Actor{
	private double degree, x, y, speed;
	private int width, height;
	private boolean left, right;
	
	
	
	
	public Snowflakes(double degree, double x, double y, double speed, int width, int height, boolean left,
			boolean right) {
		super();
		this.degree = degree;
		this.x = coordinateX();
		this.y = coordinateY();
		this.speed = speed;
		this.width = width;
		this.height = height;
		this.left = left;
		this.right = right;
	}

	public void update(int delta,GameContainer gc) {
		this.y+= delta * this.speed;
		if (this.y >=600) {
			this.y = -50;
		}
	}
	public void render(Graphics graphics) {
		graphics.fillOval((int)this.x, (int)this.y, this.width, this.height);
		
	}
	
	public double coordinateX() {
		double x = (int) (Math.random() * (Landscape.WIDTH - 0)) + 0;
		return x;
	}
	public double coordinateY() {
		double y = (int) (Math.random() * (Landscape.HEIGHT - 0)) + 0;
		return y;
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}
}
