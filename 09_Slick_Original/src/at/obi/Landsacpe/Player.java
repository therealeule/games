package at.obi.Landsacpe;



import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
public class Player implements Actor {

	private double x, y, speed;
	private Image image;
	private Shape shape;
	private List<Shape> collisionspartner;
	
	public Player(double x, double y, double speed) {
		super();
		this.collisionspartner = new ArrayList<>();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.shape =  new Circle((float)this.x,(float) this.y, 31,31);
		
		try {
			image = new Image ("images/auge.png"); 	
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void update(int delta, GameContainer gc) {
		if (gc.getInput().isKeyDown(Input.KEY_DOWN)) {
			this.y += delta*speed;
			
		}
		if (gc.getInput().isKeyDown(Input.KEY_UP)) {
			this.y -= delta*speed;
		}
		if(gc.getInput().isKeyDown(Input.KEY_LEFT)) {
			this.x -= delta*speed;
		}
		if(gc.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x += delta*speed;
		}
		

		this.shape.setX((float)this.x);
		this.shape.setY((float)this.y);
		
		for (Shape shape : collisionspartner) {
		
			if( shape.intersects(this.shape)) {
				System.out.println("collison");
		
			}
		
		}
		}
		public void addCollissionPartner(Shape s) {
			this.collisionspartner.add(s);
		}

	

	public void render(Graphics graphics) {
//		graphics.setColor(Color.red);
//		graphics.drawRect((int)this.x, (int)this.y, 50, 50);
//		graphics.setColor(Color.white);
		graphics.draw(this.shape);
		

				image.draw((int)this.x, (int)this.y, 60, 60);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
