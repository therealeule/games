package at.obi.Landsacpe;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Bullet implements Actor {
	private double x, y, speed;
	private Shape shape;
	private List<Shape> collisionspartner;
	

	public Bullet(double x, double y, double speed) {
		super();
		this.collisionspartner = new ArrayList<>();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.shape =  new Circle((float)this.x,(float) this.y, 5, 5);
	}
	
	
	public void render(Graphics graphics) {
		graphics.setColor(Color.blue);
		graphics.fillOval((int)this.x, (int)this.y, 10, 10);
		graphics.setColor(Color.white);       
		graphics.draw(this.shape);
		
	}

	@Override
	public void update(int delta, GameContainer gc) {
		this.x += delta*this.speed;
	
	this.shape.setX((float)this.x);
	this.shape.setY((float)this.y);
	
	for (Shape shape : collisionspartner) {
		
		if( shape.intersects(this.shape)) {
			System.out.println("collison");
		
		}
	
	}
	}
	public void addCollissionPartner(Shape s) {
		this.collisionspartner.add(s);
	}


	@Override
	public Shape getShape() {
		// TODO Auto-generated method stub
		return null;
	}

}
	

