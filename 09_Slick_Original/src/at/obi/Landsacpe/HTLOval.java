package at.obi.Landsacpe;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class HTLOval implements Actor{

		private double degree, x, y, speed;
		private int width, height;
		
		

		public HTLOval(double degree, double x, double y, double speed, int width, int height) {
			super();
			this.degree = degree;
			this.x = x;
			this.y = y;
			this.speed = speed;
			this.width = width;
			this.height = height;
		}

		public void update(int delta, GameContainer gc) {
			this.y+= delta * this.speed;
			if (this.y >=600) {
				this.y = 0;
			}
		}

		public void render(Graphics graphics) {
			graphics.setColor(Color.red);
			graphics.drawOval((int)this.x, (int)this.y, this.width, this.height);
			graphics.setColor(Color.white);
		}

		@Override
		public Shape getShape() {
			// TODO Auto-generated method stub
			return null;
		}
	

}
