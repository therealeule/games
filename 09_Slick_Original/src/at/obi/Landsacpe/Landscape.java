package at.obi.Landsacpe;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.tests.AnimationTest;
public class Landscape extends BasicGame{
		

		
		private HTLRectangle rectangle1;
		private HTLCicrle cicrle1;
		private HTLOval oval1;
		private HTLSquare square1;
		private Player player1;
		private Bullet bullet1;

		
		
		private List<Actor> actors;
		private Object bullet;
		
		public static final int WIDTH = 800;
	 	
		public static final int HEIGHT = 600;
		
		public Landscape() {
			super ("Landscape");
		}
		
		@Override
		public void render(GameContainer gc, Graphics graphics) throws SlickException {
			for (Actor actor : this.actors) {
				actor.render(graphics);
			}
		}
		
		@Override
		public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if (key == Input.KEY_SPACE) {
			Bullet bullet = new Bullet(this.player1.getX(), this.player1.getY(),0.2);
			this.actors.add(bullet);
 
			
		
		}
		
		}
		public void keyPressed1(int key, char c) {
			if (key == Input.KEY_ESCAPE) {
				System.exit(0);
			}
		
		
		}

		@Override
		public void init(GameContainer gc) throws SlickException {
			this.actors = new ArrayList<>();
			HTLRectangle rect = new HTLRectangle(10,100,100,0.001,50,50);
			this.actors.add(rect);
			this.actors.add(new HTLCicrle(0, 100, 100, 0.3, 50,50,false,true));
			this.actors.add(new HTLOval(10, 400, 400, 0.3, 50, 100));
			this.actors.add(new HTLSquare(0, 100, 100, 0.3, 50, 50, false, false, false, false));
			
			
			
			for (int i= 0; i<= 50; i++) {
				this.actors.add(new Snowflakes(0, 0, 0, 0.3, 8, 8, false, false));
				this.actors.add(new Snowflakes(0, 0, 0, 0.5, 16, 16, false, false));
				this.actors.add(new Snowflakes(0, 0, 0, 0.8, 24, 24, false, false));
			}
			player1 = new Player(40,400,0.5);
			this.actors.add(player1);
			this.player1.addCollissionPartner(rect.getShape());
			
//			this.bullet1.addCollissionPartner(rect.getShape());
		}

		@Override
		public void update(GameContainer gc, int delta) throws SlickException {
			for (Actor actor : this.actors) {
				actor.update(delta, gc);
			}
			
		}
		public static void main(String[] argv) {
			try {
				AppGameContainer container = new AppGameContainer(new Landscape());
				container.setDisplayMode(800,600,false);
				container.isShowingFPS();
				container.start();
			} catch (SlickException e) {
				e.printStackTrace();
			}
		
		}
		
}
